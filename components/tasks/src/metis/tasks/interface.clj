(ns metis.tasks.interface
  (:require [metis.tasks.core :as core]))

(defn build [m] (core/build m))

